﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.Applicant
{
    public class ApplicantConfigurations : IApplicantConfigurations, IDependencyConfiguration
    {
        public string[] DefaultRoles { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
