﻿namespace LendFoundry.Business.Applicant
{
    public enum AddressType
    {
        Home=1,
        Mailing=2,
        Business=3,
        Military=4
    }
}