﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Applicant
{
    public interface IApplicantRepository : IRepository<IApplicant>
    {

        Task<IApplicant> GetByApplicantId(string applicantId);

        Task SetAddresses(string applicantId, IList<IAddress> addresses);

        Task SetPhoneNumbers(string applicantId, IList<IPhoneNumber> phoneNumbers);

        Task SetEmailAddresses(string applicantId, IList<IEmailAddress> emailAddresses);

        Task SetOwners(string applicantId, IList<IOwner> owners);

        Task SetBanks(string applicantId, IList<IBankInformation> banks);

        Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest);

        Task AssociateUser(string applicantId, string userId);
        Task<List<IApplicant>> GetApplicantbyUserId(string userId);
        Task UpdateFields(string applicantId, IDictionary<string, object> fields);
    }
}
