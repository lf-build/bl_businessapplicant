﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Applicant
{
    public interface IUpdateApplicantRequest
    {
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string BusinessTaxID { get; set; }
        string BusinessType { get; set; }

        string SICCode { get; set; }
        string NAICCode { get; set; }

        string DUNSNumber { get; set; }

        string BusinessWebsite { get; set; }

        TimeBucket BusinessStartDate { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber PrimaryPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        SocialLinksRequest SocialLinks { get; set; }
        string PropertyType { get; set; }

        IList<IOwner> Owners { get; set; }

        string Industry { get; set; }

        IList<IPhoneNumber> PhoneNumbers { get; set; }

        IList<IEmailAddress> EmailAddresses { get; set; }

        IList<IAddress> Addresses { get; set; }

        string EIN { get; set; }

        string LoanPriority { get; set; }

        string BusinessLocation {  get; set; }
    }
}