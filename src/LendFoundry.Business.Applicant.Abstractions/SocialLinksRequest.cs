﻿namespace LendFoundry.Business.Applicant
{
    public class SocialLinksRequest
    {
        public string FacebookAddress { get; set; }
        public string YelpAddress { get; set; }
        public string GoogleAddress { get; set; }

    }
}