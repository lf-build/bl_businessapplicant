﻿namespace LendFoundry.Business.Applicant
{
    public enum EmailType
    {
        Personal = 1,
        Work = 2
    }
}