﻿using System;

namespace LendFoundry.Business.Applicant
{
    public interface IBankInformation
    {
        string BankId { get; set; }
        string BankName { get; set; }
        string AccountNumber { get; set; }
        string AccountHolderName { get; set; }
        string IfscCode { get; set; }
        AccountType AccountType { get; set; }
        bool IsVerified { get; set; }
        DateTimeOffset? VerifiedDate { get; set; }
    }
}