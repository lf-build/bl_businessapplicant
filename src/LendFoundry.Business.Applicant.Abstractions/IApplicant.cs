﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Applicant
{
    public interface IApplicant : IAggregate
    {
        string UserId { get; set; }
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string BusinessTaxID { get; set; }
        string BusinessType { get; set; }

        string SICCode { get; set; }
        string NAICCode { get; set; }
        List<string> SecondaryNAICS { get; set; }
        string DUNSNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        IList<IAddress> Addresses { get; set; }

        string BusinessWebsite { get; set; }

        TimeBucket BusinessStartDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankInformation, BankInformation>))]
        IList<IBankInformation> BankInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        IList<IOwner> Owners { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber PrimaryPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        SocialLinksRequest SocialLinks { get; set; }
        string PropertyType { get; set; }
        string Industry { get; set; }

        string EIN { get; set; }

        string LoanPriority { get; set; }

        string BusinessLocation { get; set; }
    }
}