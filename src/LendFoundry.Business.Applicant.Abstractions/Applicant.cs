﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Applicant
{
    public class Applicant : Aggregate, IApplicant
    {
        #region Constructors

        public Applicant()
        {
        }

        public Applicant(IApplicantRequest applicant)
        {
            if (applicant == null)
                throw new ArgumentNullException(nameof(applicant));

            Id = applicant.Id;
            UserId = applicant.UserId;
            DBA = applicant.DBA;
            BusinessTaxID = applicant.BusinessTaxID;
            BusinessType = applicant.BusinessType;
            SICCode = applicant.SICCode;
            NAICCode = applicant.NAICCode;
            SecondaryNAICS = applicant.SecondaryNAICS;
            DUNSNumber = applicant.DUNSNumber;
            BusinessWebsite = applicant.BusinessWebsite;
            BusinessStartDate = applicant.BusinessStartDate;
            Owners = applicant.Owners;
            SocialLinks = applicant.SocialLinks;
            PrimaryFax = applicant.PrimaryFax;
            PrimaryPhone = applicant.PrimaryPhone;
            PrimaryEmail = applicant.PrimaryEmail;
            PrimaryAddress = applicant.PrimaryAddress;
            EmailAddresses = applicant.EmailAddresses;
            LegalBusinessName = applicant.LegalBusinessName;
            PhoneNumbers = applicant.PhoneNumbers;
            Addresses = applicant.Addresses;
            BankInformation = applicant.BankInformation;
            PropertyType = applicant.PropertyType;
            Industry = applicant.Industry;
            EIN = applicant.EIN;
            LoanPriority = applicant.LoanPriority;
            BusinessLocation = applicant.BusinessLocation;
        }

        #endregion Constructors

        #region Public Properties

        public string UserId { get; set; }
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string BusinessTaxID { get; set; }
        public string BusinessType { get; set; }

        public string SICCode { get; set; }
        public string NAICCode { get; set; }
        public List<string> SecondaryNAICS { get; set; }
        public string DUNSNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public IList<IAddress> Addresses { get; set; }

        public string BusinessWebsite { get; set; }

        public TimeBucket BusinessStartDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankInformation, BankInformation>))]
        public IList<IBankInformation> BankInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        public SocialLinksRequest SocialLinks { get; set; }
        public string PrimaryFax { get; set; }

        public string Industry { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber PrimaryPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public string PropertyType { get; set; }

        public string EIN { get; set; }

        public string BusinessLocation { get; set; }

        public string LoanPriority { get; set; }
        
        #endregion Public Properties
    }
}