﻿namespace LendFoundry.Business.Applicant
{
    public class PhoneNumber : IPhoneNumber
    {
        #region Constructors

        public PhoneNumber()
        {
        }

        #endregion Constructors

        #region Public Properties

        public string PhoneId { get; set; }
        public string Phone { get; set; }
        public string CountryCode { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsDefault { get; set; }

        #endregion Public Properties
    }
}