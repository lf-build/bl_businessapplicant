﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantRequest : Applicant, IApplicantRequest
    {
        #region Constructors
        public ApplicantRequest() { }

        public ApplicantRequest(IApplicantRequest applicantRequest) : base(applicantRequest)
        {
          
            ContactFirstName = applicantRequest.ContactFirstName;
            ContactLastName = applicantRequest.ContactLastName;
        }
        #endregion

        #region Public Properties
      
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        #endregion

    }
}
