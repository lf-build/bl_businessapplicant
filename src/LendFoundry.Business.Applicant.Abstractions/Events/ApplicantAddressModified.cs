﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantAddressModified
    {
        public IApplicant Applicant { get; set; }
    }
}
