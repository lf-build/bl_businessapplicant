﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantPhoneNumberModified
    {
        public IApplicant Applicant { get; set; }
    }
}
