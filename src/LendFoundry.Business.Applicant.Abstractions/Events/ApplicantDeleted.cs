﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantDeleted
    {
        public IApplicant Applicant { get; set; }
    }
}
