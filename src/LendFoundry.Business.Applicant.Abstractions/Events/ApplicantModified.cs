﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantModified
    {
        public IApplicant Applicant { get; set; }
    }
}