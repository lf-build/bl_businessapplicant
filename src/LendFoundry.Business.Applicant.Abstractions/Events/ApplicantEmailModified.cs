﻿namespace LendFoundry.Business.Applicant
{
    public class ApplicantEmailModified
    {
        public IApplicant Applicant { get; set; }
    }
}
