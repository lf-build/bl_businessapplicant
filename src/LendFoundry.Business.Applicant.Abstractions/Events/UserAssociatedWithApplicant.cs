﻿namespace LendFoundry.Business.Applicant
{
    public class UserAssociatedWithApplicant
    {
        public string ApplicantId { get; set; }
        public string UserId { get; set; }
    }
}
