﻿
namespace LendFoundry.Business.Applicant
{
    public class ApplicantCreated
    {
        public IApplicant Applicant { get; set; }
    }
}
