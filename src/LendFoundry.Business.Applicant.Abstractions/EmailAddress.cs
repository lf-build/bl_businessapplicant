﻿namespace LendFoundry.Business.Applicant
{
    public class EmailAddress : IEmailAddress
    {
        #region Public Properties

        public string Id { get; set; }
        public string Email { get; set; }
        public EmailType EmailType { get; set; }
        public bool IsDefault { get; set; }

        #endregion Public Properties
    }
}