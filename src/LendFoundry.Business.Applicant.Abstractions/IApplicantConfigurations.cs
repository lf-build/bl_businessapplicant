﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.Applicant
{
    public interface IApplicantConfigurations:IDependencyConfiguration
    {
        string[] DefaultRoles { get; set; }
        string ConnectionString { get; set; }
    }
}