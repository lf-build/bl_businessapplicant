﻿using System;

namespace LendFoundry.Business.Applicant
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-applicant";

    }
}
