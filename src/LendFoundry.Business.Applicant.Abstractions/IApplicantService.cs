﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Applicant
{
    public interface IApplicantService
    {
        Task<IApplicant> Add(IApplicantRequest applicant);
        Task<IApplicant> Get(string applicantId);
        Task Delete(string applicantId);
        Task AssociateUser(string applicantId, string userId);
        Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest applicantRequest);
        Task SetAddresses(string applicantId, List<IAddress> addresses);
        Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers);
        Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses);
        Task SetOwner(string applicantId, List<IOwner> ownres);
        Task SetBanks(string applicantId, List<IBankInformation> banks);
        Task<List<IApplicant>> GetApplicantbyUserId(string userId);
        Task<IApplicant> UpdateFields(string applicantId, IDictionary<string, object> fields);
        Task SetPrimary(string applicantId, string OwnerId);
        Task<IOwner> GetPrimaryOwner(string applicantId);
    }
}
