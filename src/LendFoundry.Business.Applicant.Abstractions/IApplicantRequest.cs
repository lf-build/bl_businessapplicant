﻿namespace LendFoundry.Business.Applicant
{
    public interface IApplicantRequest :IApplicant
    {
        
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }

    }
}