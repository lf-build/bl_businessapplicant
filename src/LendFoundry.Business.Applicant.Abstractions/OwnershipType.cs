﻿namespace LendFoundry.Business.Applicant
{
    public enum OwnershipType
    {
        General,
        Limited,
        JointVenture
    }
}