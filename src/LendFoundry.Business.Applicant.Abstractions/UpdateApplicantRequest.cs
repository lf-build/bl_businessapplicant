﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Applicant
{
    public class UpdateApplicantRequest : IUpdateApplicantRequest
    {
        #region Constructors
        public UpdateApplicantRequest() { }

        public UpdateApplicantRequest(IApplicantRequest applicantRequest)
        {
            LegalBusinessName = applicantRequest.LegalBusinessName;
            DBA = applicantRequest.DBA;
            BusinessTaxID = applicantRequest.BusinessTaxID;
            BusinessType = applicantRequest.BusinessType;
            SICCode = applicantRequest.SICCode;
            NAICCode = applicantRequest.NAICCode;
            DUNSNumber = applicantRequest.DUNSNumber;
            BusinessWebsite = applicantRequest.BusinessWebsite;
            BusinessStartDate = applicantRequest.BusinessStartDate;
            PrimaryFax = applicantRequest.PrimaryFax;
            PrimaryPhone = applicantRequest.PrimaryPhone;
            PrimaryEmail = applicantRequest.PrimaryEmail;
            PrimaryAddress = applicantRequest.PrimaryAddress;
            SocialLinks = applicantRequest.SocialLinks;
            PropertyType = applicantRequest.PropertyType;
            Owners = applicantRequest.Owners;
            Industry = applicantRequest.Industry;
            PhoneNumbers = applicantRequest.PhoneNumbers;
            EmailAddresses = applicantRequest.EmailAddresses;
            Addresses = applicantRequest.Addresses;
            EIN = applicantRequest.EIN;
            LoanPriority = applicantRequest.LoanPriority;
            BusinessLocation = applicantRequest.BusinessLocation;
        }
        #endregion

        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string BusinessTaxID { get; set; }
        public string BusinessType { get; set; }

        public string SICCode { get; set; }
        public string NAICCode { get; set; }

        public string DUNSNumber { get; set; }

        public string BusinessWebsite { get; set; }

        public TimeBucket BusinessStartDate { get; set; }

        public string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber PrimaryPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public SocialLinksRequest SocialLinks { get; set; }
        public string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        public string Industry { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public IList<IAddress> Addresses { get; set; }
        public string EIN { get; set; }
        public string LoanPriority { get; set; }

        public string BusinessLocation { get; set; }
    }
}