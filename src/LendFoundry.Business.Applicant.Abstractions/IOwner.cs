﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Applicant
{
    public interface IOwner
    {
        string OwnerId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
        string DOB { get; set; }
        OwnershipType OwnershipType { get; set; }
        string OwnershipPercentage { get; set; }
        string Designation { get; set; }
        string SelfReportedFico { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        IList<IAddress> Addresses { get; set; }
        bool? IsPrimary { get; set; }
    }
}