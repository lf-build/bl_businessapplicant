﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.EventHub.Client;
using LendFoundry.Business.Applicant.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Security.Encryption;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Runtime;
using System;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Applicant.Api
{
    internal class Startup
    {
        
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "BusinessApplicant"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Business.Applicant.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<ApplicantConfigurations>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IApplicantConfigurations, ApplicantConfigurations>();
            services.AddTransient<IApplicantConfigurations>(p => p.GetService<IConfigurationService<ApplicantConfigurations>>().Get());
            services.AddTransient<IApplicantRepository, ApplicantRepository>();
            services.AddTransient<IApplicantService, ApplicantService>();
            //services.AddSwaggerGen();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddEncryptionHandler();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
            
// Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "BusinessApplicant Service");
            });
            app.UseErrorHandling();
            app.UseRequestLogging();
          
            app.UseMvc();
        }
    }

  
}

