﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
using Microsoft.AspNetCore.Mvc;
namespace LendFoundry.Business.Applicant.Api.Controllers
{
    /// <summary>
    /// Applicant Service
    /// </summary>
    [Route("/")]
    public class ApplicantController : ExtendedController
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="applicantService"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ApplicantController(IApplicantService applicantService)
        {
            if (applicantService == null)
                throw new ArgumentNullException(nameof(applicantService));

            ApplicantService = applicantService;
        }

        #endregion Constructors

        #region Private Properties

        private IApplicantService ApplicantService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Add new applicant
        /// </summary>
        /// <param name="applicantRequest"></param>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> Add([FromBody]ApplicantRequest applicantRequest)
        {
            try
            {
                return Ok(await ApplicantService.Add(applicantRequest));
            }
            catch (UsernameAlreadyExists exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UnableToCreateUserException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            //return await ExecuteAsync(async () => Ok(await ApplicantService.Add(applicantRequest)));
        }

        /// <summary>
        /// Get application by applicantId
        /// </summary>
        /// <param name="applicantId">ApplicantId</param>
        [HttpGet("/{applicantId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> Get(string applicantId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.Get(applicantId)));
        }
        
        /// <summary>
        /// Update Applicant
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="applicantRequest"></param>
        [HttpPut("/{applicantId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> UpdateApplicant(string applicantId, [FromBody]UpdateApplicantRequest applicantRequest)
        {
            return Ok(await ApplicantService.UpdateApplicant(applicantId, applicantRequest));
        }

        /// <summary>
        /// Delete Applicant
        /// </summary>
        /// <param name="applicantId"></param>
        [HttpDelete("/{applicantId}")]
        public async Task<IActionResult> Delete(string applicantId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.Delete(applicantId); return NoContentResult; });
        }

        /// <summary>
        /// SetAddresses
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="addresses"></param>
        [HttpPut("/{applicantId}/addresses")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetAddresses(string applicantId, [FromBody]List<Address> addresses)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetAddresses(applicantId, new List<IAddress>(addresses)); return NoContentResult; });
        }

        /// <summary>
        /// SetEmailAddresses
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="emailAddresses"></param>
        [HttpPut("/{applicantId}/emailaddresses")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetEmailAddresses(string applicantId, [FromBody]List<EmailAddress> emailAddresses)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetEmailAddresses(applicantId, new List<IEmailAddress>(emailAddresses)); return NoContentResult; });
        }

        /// <summary>
        /// SetPhoneNumbers
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="phoneNumbers"></param>
        [HttpPut("/{applicantId}/phonenumbers")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetPhoneNumbers(string applicantId, [FromBody]List<PhoneNumber> phoneNumbers)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetPhoneNumbers(applicantId, new List<IPhoneNumber>(phoneNumbers)); return NoContentResult; });
        }

        /// <summary>
        /// SetOwners
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="owners"></param>
        [HttpPut("/{applicantId}/owners")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetOwners(string applicantId, [FromBody]List<Owner> owners)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetOwner(applicantId, new List<IOwner>(owners)); return NoContentResult; });
        }

        /// <summary>
        /// SetBanks
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="banks"></param>
        [HttpPut("/{applicantId}/banks")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetBanks(string applicantId, [FromBody]List<BankInformation> banks)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetBanks(applicantId, new List<IBankInformation>(banks)); return NoContentResult; });
        }

        /// <summary>
        /// AssociateUser
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="userId"></param>
        [HttpPut("/{applicantId}/associate/user/{userId}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> AssociateUser(string applicantId, string userId)
        {
            return await ExecuteAsync(async () =>
            {
                try 
                {
                    { await ApplicantService.AssociateUser(applicantId, userId); return NoContentResult; }
                }
                catch (UserAlreadyAssociatedException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        ///  GetApplicantbyUserId
        /// </summary>
        /// <param name="userId"></param>
        [HttpGet("applicant/by/{userId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(List<IApplicant>))]
        public async Task<IActionResult> GetApplicantbyUserId(string userId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.GetApplicantbyUserId(userId)));
        }

        /// <summary>
        /// UpdateFields
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="fields"></param>
        [HttpPut("{applicantId}/update/fields")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> UpdateFields(string applicantId, [FromBody]Dictionary<string, object> fields)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.UpdateFields(applicantId, fields)));
        }

        /// <summary>
        /// SetPrimary
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="ownerId"></param>
        [HttpPut("/{applicantId}/{ownerId}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetPrimary(string applicantId, string ownerId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    { await ApplicantService.SetPrimary(applicantId, ownerId); return NoContentResult; }
                }
                catch (UserAlreadyAssociatedException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
     
        /// <summary>
        ///  GetPrimaryOwner
        /// </summary>
        /// <param name="applicantId"></param>
        [HttpGet("{applicantId}/primary")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOwner))]
        public async Task<IActionResult> GetPrimaryOwner(string applicantId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.GetPrimaryOwner(applicantId)));
        }

        #endregion Public Methods
    }
}