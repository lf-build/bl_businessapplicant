﻿using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Applicant.Client
{
   
    public static class ApplicantServiceClientExtensions
    {
        public static IServiceCollection AddApplicantService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddSingleton<IApplicantServiceClientFactory>(p => new ApplicantServiceClientFactory(p, endpoint, port));
            services.AddSingleton(p => p.GetService<IApplicantServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IApplicantServiceClientFactory>(p => new ApplicantServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IApplicantServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services)
        {
            services.AddSingleton<IApplicantServiceClientFactory>(p => new ApplicantServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IApplicantServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
