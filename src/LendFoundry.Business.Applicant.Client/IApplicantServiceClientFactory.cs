﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Applicant.Client
{
    public interface IApplicantServiceClientFactory
    {
        IApplicantService Create(ITokenReader reader);
    }
}
