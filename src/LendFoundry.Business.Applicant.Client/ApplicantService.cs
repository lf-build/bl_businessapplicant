﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using LendFoundry.Foundation.Client;


namespace LendFoundry.Business.Applicant.Client
{
    public class ApplicantService : IApplicantService
    {
        #region Constructors
        public ApplicantService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties
        private IServiceClient Client { get; }
        #endregion

        #region Public Methods
        public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {
            var request = new RestRequest("/", Method.POST);
            request.AddJsonBody(applicantRequest);
            return await Client.ExecuteAsync<Applicant>(request);
        }


        public async Task<IApplicant> Get(string applicantId)
        {
            var request = new RestRequest("/{applicantId}", Method.GET);
            request.AddUrlSegment("applicantId", applicantId);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task Delete(string applicantId)
        {
            var request = new RestRequest("/{applicantId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest applicantRequest)
        {
            var request = new RestRequest("/{applicantId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(applicantRequest);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task SetAddresses(string applicantId, List<IAddress> addresses)
        {
            var request = new RestRequest("{applicantId}/address", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(addresses);
            await Client.ExecuteAsync(request);
        }

        public async Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            var request = new RestRequest("{applicantId}/phonenumbers", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(phoneNumbers);
            await Client.ExecuteAsync(request);
        }

        public async Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            var request = new RestRequest("{applicantId}/emailaddress", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(emailAddresses);
            await Client.ExecuteAsync(request);
        }

        public async Task SetOwner(string applicantId, List<IOwner> owners)
        {
            var request = new RestRequest("{applicantId}/owners", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(owners);
            await Client.ExecuteAsync(request);
        }

        public async Task SetBanks(string applicantId, List<IBankInformation> banks)
        {
            var request = new RestRequest("{applicantId}/banks", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(banks);
            await Client.ExecuteAsync(request);
        }
        public async Task AssociateUser(string applicantId, string userId)
        {
            var request = new RestRequest("{applicantId}/associate/user/{userId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("userId", userId);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IApplicant>> GetApplicantbyUserId(string userId)
        {
            var request = new RestRequest("applicant/by/{userId}", Method.GET);
            request.AddUrlSegment("userId", userId);
            return new List<IApplicant>(await Client.ExecuteAsync<List<Applicant>>(request));
        }

        public async Task<IApplicant> UpdateFields(string applicantId, IDictionary<string, object> fields)
        {
            var request = new RestRequest("{applicantId}/update/fields", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(fields);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task SetPrimary(string applicantId, string ownerId)
        {
            var request = new RestRequest("{applicantId}/{ownerId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("ownerId", ownerId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IOwner> GetPrimaryOwner(string applicantId)
        {
            var request = new RestRequest("/{applicantId}/primary", Method.GET);
            request.AddUrlSegment("applicantId", applicantId);
            return await Client.ExecuteAsync<Owner>(request);
        }
        #endregion
    }
}