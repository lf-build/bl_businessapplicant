﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Encryption;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.Business.Applicant.Persistence
{
    public class ApplicantRepository : MongoRepository<IApplicant, Applicant>, IApplicantRepository
    {
        #region Constructors

        static ApplicantRepository()
        {
           
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                var type = typeof(Address);
                map.MapProperty(p => p.AddressType).SetSerializer(new EnumSerializer<AddressType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber>(map =>
            {
                map.AutoMap();
                var type = typeof(PhoneNumber);
                map.MapProperty(p => p.PhoneType).SetSerializer(new EnumSerializer<PhoneType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<EmailAddress>(map =>
            {
                map.AutoMap();
                var type = typeof(EmailAddress);
                map.MapProperty(p => p.EmailType).SetSerializer(new EnumSerializer<EmailType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankInformation>(map =>
            {
                map.AutoMap();
                var type = typeof(BankInformation);
                map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<AccountType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });          
            BsonClassMap.RegisterClassMap<SocialLinksRequest>(map =>
            {
                map.AutoMap();
                var type = typeof(SocialLinksRequest);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        #endregion Constructors

        #region Public Methods

        public ApplicantRepository(Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration,IEncryptionService encrypterService)
            : base(tenantService, configuration, "applicants")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Applicant)))
            {
                BsonClassMap.RegisterClassMap<Applicant>(map =>
            {
                map.AutoMap();
                map.MapProperty(a => a.Addresses).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.PhoneNumbers).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.PrimaryEmail).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.PrimaryPhone).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.Owners).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.EmailAddresses).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.BankInformation).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.LoanPriority).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.BusinessLocation).SetIgnoreIfDefault(true);
                map.MapMember(m => m.BusinessTaxID).SetSerializer(new BsonEncryptor<string,string>(encrypterService));            
                var type = typeof(Applicant);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(Owner)))
            {
                BsonClassMap.RegisterClassMap<Owner>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.SSN).SetSerializer(new BsonEncryptor<string,string>(encrypterService));
                    map.MapMember(m => m.DOB).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(Owner);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }

            CreateIndexIfNotExists("tenantid-applicantId", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
            CreateIndexIfNotExists("tenantid-userid",Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i=>i.UserId));

        }

        public async Task<IApplicant> GetByApplicantId(string applicantId)
        {
            ObjectId obj;
            var isValid = ObjectId.TryParse(applicantId, out obj);
            if (!isValid)
                return null;
            return await Query.FirstOrDefaultAsync(applicant =>  applicant.Id == applicantId);
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                     a.Id == applicantId), Builders<IApplicant>.Update.Set(a => a.UserId, userId));
        }

        public async Task SetAddresses(string applicantId, IList<IAddress> addresses)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.Addresses, addresses));
        }

        public async Task SetPhoneNumbers(string applicantId, IList<IPhoneNumber> phoneNumbers)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.PhoneNumbers, phoneNumbers));
        }

        public async Task SetOwners(string applicantId, IList<IOwner> owners)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.Owners, owners));
        }

        public async Task SetBanks(string applicantId, IList<IBankInformation> banks)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.BankInformation, banks));
        }

        public async Task SetEmailAddresses(string applicantId, IList<IEmailAddress> emailAddresses)
        {
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.EmailAddresses, emailAddresses));
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {           
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.LegalBusinessName, updateApplicantRequest.LegalBusinessName)
                .Set(a => a.DBA, updateApplicantRequest.DBA)
                .Set(a => a.BusinessTaxID, updateApplicantRequest.BusinessTaxID)
                .Set(a => a.BusinessType, updateApplicantRequest.BusinessType)
                .Set(a => a.SICCode, updateApplicantRequest.SICCode)
                .Set(a => a.DUNSNumber, updateApplicantRequest.DUNSNumber)
                .Set(a => a.BusinessWebsite, updateApplicantRequest.BusinessWebsite)
                .Set(a => a.PrimaryEmail, updateApplicantRequest.PrimaryEmail)
                .Set(a => a.PrimaryPhone, updateApplicantRequest.PrimaryPhone)
                .Set(a => a.PrimaryAddress, updateApplicantRequest.PrimaryAddress)
                .Set(a => a.SocialLinks, updateApplicantRequest.SocialLinks)
                .Set(a => a.BusinessStartDate, updateApplicantRequest.BusinessStartDate)
                .Set(a => a.PrimaryFax, updateApplicantRequest.PrimaryFax)
                .Set(a => a.Industry, updateApplicantRequest.Industry)
                .Set(a => a.NAICCode, updateApplicantRequest.NAICCode)
                .Set(a => a.PropertyType, updateApplicantRequest.PropertyType)
                .Set(a => a.EmailAddresses, updateApplicantRequest.EmailAddresses)
                .Set(a => a.PhoneNumbers, updateApplicantRequest.PhoneNumbers)
                .Set(a => a.Addresses, updateApplicantRequest.Addresses)
                .Set(a => a.Owners, updateApplicantRequest.Owners)
                .Set(a => a.EIN, updateApplicantRequest.EIN)
                .Set(a => a.LoanPriority, updateApplicantRequest.LoanPriority)
                .Set(a => a.BusinessLocation, updateApplicantRequest.BusinessLocation));

            var applicant = await GetByApplicantId(applicantId);
            return applicant;
        }

        public async Task<List<IApplicant>> GetApplicantbyUserId(string userId)
        {
            ObjectId obj;
            var isValid = ObjectId.TryParse(userId, out obj);
            if (!isValid)
                return null;
            return await Query.Where(a => a.TenantId == TenantService.Current.Id && a.UserId == userId).ToListAsync();
        }

        public async Task UpdateFields(string applicantId,IDictionary<string,object> fields)
        {
            var filter = Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                     a.Id == applicantId);
            var firstField=fields.FirstOrDefault();
            fields.Remove(firstField.Key);
            var update = Builders<IApplicant>.Update.Set(firstField.Key, firstField.Value);
            if (fields.Count > 0)
            {
                update = update.SetDictionary(fields);
            }
            await Collection.UpdateOneAsync(filter,update);
        }

        #endregion Public Methods
    }
}