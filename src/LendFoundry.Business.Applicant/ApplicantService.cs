﻿
using LendFoundry.EventHub;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Business.Applicant
{
    #region Constructor

    public class ApplicantService : IApplicantService
    {
        public ApplicantService(IApplicantRepository applicantRepository,IEventHubClient eventHubClient)
        {
            EventHubClient = eventHubClient;
            ApplicantRepository = applicantRepository;
        }

        #endregion Constructor

        #region Private Properties

        private IApplicantRepository ApplicantRepository { get; }
        private IEventHubClient EventHubClient { get; }

        #endregion Private Properties

        public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {
            if (applicantRequest == null)
                throw new ArgumentNullException(nameof(applicantRequest));

            if (applicantRequest.Addresses != null)
            {
                foreach (var address in applicantRequest.Addresses)
                {
                    address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                }
            }
            if (applicantRequest.PhoneNumbers != null)
            {
                foreach (var phone in applicantRequest.PhoneNumbers)
                {
                    phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                }
            }
            if (applicantRequest.EmailAddresses != null)
            {
                foreach (var email in applicantRequest.EmailAddresses)
                {
                    email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                }
            }
            if (applicantRequest.BankInformation != null)
            {
                if (applicantRequest.BankInformation != null && applicantRequest.BankInformation.Any())
                {
                    foreach (var bank in applicantRequest.BankInformation)
                    {
                        bank.BankId = string.IsNullOrWhiteSpace(bank.BankId) ? GenerateUniqueId() : bank.BankId;
                    }
                }
            }
            if (applicantRequest.Owners != null && applicantRequest.Owners.Any())
            {
                foreach (var owner in applicantRequest.Owners)
                {
                    if (owner.IsPrimary.HasValue)
                    {
                        owner.IsPrimary = owner.IsPrimary.Value;
                    }
                    owner.OwnerId = string.IsNullOrWhiteSpace(owner.OwnerId) ? GenerateUniqueId() : owner.OwnerId;
                    if (owner.Addresses != null && owner.Addresses.Any())
                    {
                        foreach (var address in owner.Addresses)
                        {
                            address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                        }
                    }
                    if (owner.PhoneNumbers != null && owner.PhoneNumbers.Any())
                    {
                        foreach (var phone in owner.PhoneNumbers)
                        {
                            phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                        }
                    }
                    if (owner.EmailAddresses != null && owner.EmailAddresses.Any())
                    {
                        foreach (var email in owner.EmailAddresses)
                        {
                            email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                        }
                    }
                }
            }

            IApplicant applicantModel = new Applicant(applicantRequest);           
            ApplicantRepository.Add(applicantModel);
            applicantRequest.Id = applicantModel.Id;
            await EventHubClient.Publish(new ApplicantCreated() { Applicant = applicantModel });
            return new Applicant(applicantRequest);
        }

        public async Task<IApplicant> Get(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            return applicant;
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {
            if (updateApplicantRequest.Addresses != null && updateApplicantRequest.Addresses.Any())
            {
                foreach (var address in updateApplicantRequest.Addresses)
                {
                    address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                }
            }
            if (updateApplicantRequest.PhoneNumbers != null && updateApplicantRequest.PhoneNumbers.Any())
            {
                foreach (var phone in updateApplicantRequest.PhoneNumbers)
                {
                    phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                }
            }
            if (updateApplicantRequest.EmailAddresses != null && updateApplicantRequest.EmailAddresses.Any())
            {
                foreach (var email in updateApplicantRequest.EmailAddresses)
                {
                    email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                }
            }
            if (updateApplicantRequest.Owners != null && updateApplicantRequest.Owners.Any())
            {
                foreach (var owner in updateApplicantRequest.Owners)
                {
                    if (owner.IsPrimary.HasValue)
                    {
                        owner.IsPrimary = owner.IsPrimary.Value;
                    }
                    owner.OwnerId = string.IsNullOrWhiteSpace(owner.OwnerId) ? GenerateUniqueId() : owner.OwnerId;
                    if (owner.Addresses != null && owner.Addresses.Any())
                    {
                        foreach (var address in owner.Addresses)
                        {
                            address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                        }
                    }
                    if (owner.PhoneNumbers != null && owner.PhoneNumbers.Any())
                    {
                        foreach (var phone in owner.PhoneNumbers)
                        {
                            phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                        }
                    }
                    if (owner.EmailAddresses != null && owner.EmailAddresses.Any())
                    {
                        foreach (var email in owner.EmailAddresses)
                        {
                            email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                        }
                    }
                }
            }
            var applicant = await GetApplicant(applicantId);
            var updatedApplicant = await ApplicantRepository.UpdateApplicant(applicantId, updateApplicantRequest);
            await EventHubClient.Publish(new ApplicantModified() { Applicant = updatedApplicant });
            return updatedApplicant;
        }

        public async Task Delete(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            ApplicantRepository.Remove(applicant);
            await EventHubClient.Publish(new ApplicantDeleted() { Applicant = applicant });
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userId));

            var applicant = await GetApplicant(applicantId);
            
            if (!string.IsNullOrEmpty(applicant.UserId))
            {
                throw new UserAlreadyAssociatedException($"Applicant {applicantId} already has associated user");
            }

            await ApplicantRepository.AssociateUser(applicantId, userId);
            await EventHubClient.Publish(new UserAssociatedWithApplicant() { ApplicantId = applicantId, UserId = userId });
        }

        public async Task SetAddresses(string applicantId, List<IAddress> addresses)
        {
            EnsureInputIsValid(addresses);
            
            var applicant = await GetApplicant(applicantId);
            var lstAddress = applicant.Addresses ?? new List<IAddress>();

            foreach (var address in addresses)
            {
                if (string.IsNullOrWhiteSpace(address.AddressId))
                {
                    address.AddressId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentAddress = lstAddress.FirstOrDefault(a => a.AddressId == address.AddressId);
                    if (currentAddress == null)
                        throw new NotFoundException($"Address  {address.AddressId} for Applicant {applicantId} not found");

                    lstAddress.Remove(currentAddress);
                }
                lstAddress.Add(address);
            }
            applicant.Addresses = lstAddress;

            await ApplicantRepository.SetAddresses(applicantId, lstAddress);
            await EventHubClient.Publish(new ApplicantAddressModified() { Applicant = applicant });
        }

        public async Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            EnsureInputIsValid(phoneNumbers);
            var applicant = await GetApplicant(applicantId);
            var lstPhoneNumber = applicant.PhoneNumbers ?? new List<IPhoneNumber>();

            foreach (var phone in phoneNumbers)
            {
                if (string.IsNullOrWhiteSpace(phone.PhoneId))
                {
                    phone.PhoneId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentPhone = lstPhoneNumber.FirstOrDefault(a => a.PhoneId == phone.PhoneId);
                    if (currentPhone == null)
                        throw new NotFoundException($"Phone Number {nameof(currentPhone.Phone)} for Applicant {applicantId} not found");
                    lstPhoneNumber.Remove(currentPhone);
                }
                lstPhoneNumber.Add(phone);
            }
            applicant.PhoneNumbers = lstPhoneNumber;

            await ApplicantRepository.SetPhoneNumbers(applicantId, lstPhoneNumber);
            await EventHubClient.Publish(new ApplicantPhoneNumberModified() { Applicant = applicant });
        }

        public async Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            EnsureInputIsValid(emailAddresses);

            var applicant = await GetApplicant(applicantId);
            var lstEmailAddress = applicant.EmailAddresses ?? new List<IEmailAddress>();

            foreach (var emailAddress in emailAddresses)
            {
                if (string.IsNullOrWhiteSpace(emailAddress.Id))
                {
                    emailAddress.Id = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentEmail = lstEmailAddress.FirstOrDefault(a => a.Id == emailAddress.Id);
                    if (currentEmail == null)
                        throw new NotFoundException($"Email Address {nameof(emailAddress.Email)} for Applicant {applicantId} not found");
                    lstEmailAddress.Remove(currentEmail);
                }
                lstEmailAddress.Add(emailAddress);
            }
            applicant.EmailAddresses = lstEmailAddress;

            await ApplicantRepository.SetEmailAddresses(applicantId, lstEmailAddress);
            await EventHubClient.Publish(new ApplicantEmailModified() { Applicant = applicant });
        }

        public async Task SetOwner(string applicantId, List<IOwner> owners)
        {
            if (owners == null || owners.Count <= 0)
                throw new ArgumentNullException(nameof(owners));
            
            EnsureInputIsValid(owners);

            var applicant = await GetApplicant(applicantId);
            var lstOwnerDetail = applicant.Owners ?? new List<IOwner>();

            foreach (var owner in owners)
            {
                if (string.IsNullOrWhiteSpace(owner.OwnerId))
                {
                    owner.OwnerId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentEmployment = lstOwnerDetail.FirstOrDefault(a => a.OwnerId == owner.OwnerId);
                    if (currentEmployment == null)
                        throw new NotFoundException($"Employment {nameof(owner.FirstName)} for Applicant {applicantId} not found");
                    lstOwnerDetail.Remove(currentEmployment);
                }
                applicant.Owners = lstOwnerDetail;               
                lstOwnerDetail.Add(owner);
            }
            
            await ApplicantRepository.SetOwners(applicantId, lstOwnerDetail);
            await EventHubClient.Publish(new ApplicantPhoneNumberModified() { Applicant = applicant });
        }

        public async Task SetPrimary(string applicantId, string ownerId)
        {
            if (string.IsNullOrWhiteSpace(ownerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(ownerId));

            var applicant = await GetApplicant(applicantId);
            if (applicant.Owners != null && applicant.Owners.Count > 0)
            {
                if (applicant.Owners.FirstOrDefault(item => item.OwnerId == ownerId) != null)
                {
                    foreach (var owner in applicant.Owners)
                    {
                        owner.IsPrimary = owner.OwnerId == ownerId;
                    }
                    await ApplicantRepository.SetOwners(applicantId, applicant.Owners.ToList());
                }
                else
                {
                    throw new NotFoundException($"Owner {ownerId} not found");
                }
            }
        }

        public async Task SetBanks(string applicantId, List<IBankInformation> banks)
        {
            if (banks == null || banks.Count <= 0)
                throw new ArgumentNullException(nameof(banks));

            EnsureInputIsValid(banks);

            var applicant = await GetApplicant(applicantId);
            var lstBank = applicant.BankInformation ?? new List<IBankInformation>();

            foreach (var bank in banks)
            {
                if (string.IsNullOrWhiteSpace(bank.BankId))
                {
                    bank.BankId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentBank = lstBank.FirstOrDefault(a => a.BankId == bank.BankId);
                    if (currentBank == null)
                        throw new NotFoundException($"Bank with account number {nameof(bank.AccountNumber)} for Applicant {applicantId} not found");
                    lstBank.Remove(currentBank);
                }
                lstBank.Add(bank);
            }
            applicant.BankInformation = lstBank;

            await ApplicantRepository.SetBanks(applicantId, lstBank);
            await EventHubClient.Publish(new ApplicantEmailModified() { Applicant = applicant });
        }

        public async Task<List<IApplicant>> GetApplicantbyUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Argument is null or whitespace", nameof(userId));
            
            var applicants = await ApplicantRepository.GetApplicantbyUserId(userId);
            if (applicants == null)
                throw new NotFoundException($"User {userId} not found");
            return applicants;
        }

        public async Task<IApplicant> UpdateFields(string applicantId, IDictionary<string, object> fields)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentNullException(nameof(applicantId));

            if (fields == null || fields.Count == 0)
                throw new ArgumentNullException(nameof(fields));
            await ApplicantRepository.UpdateFields(applicantId, fields);
            return await Get(applicantId);
        }

        public async Task<IOwner> GetPrimaryOwner(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.Get(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            if (applicant.Owners != null && applicant.Owners.Count > 0)
            {
                return applicant.Owners.FirstOrDefault(item => item.IsPrimary == true);
            }

            return null;
        }

        private async Task<IApplicant> GetApplicant (string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");
            return applicant;
        }


        #region Private Methods - Validations
        private void EnsureInputIsValid(IList<IBankInformation> banks)
        {
            if (banks == null || !banks.Any())
                throw new ArgumentNullException($"Applicant {nameof(banks)} is mandatory");

            foreach (var bank in banks)
            {
                if (string.IsNullOrWhiteSpace(bank.BankName))
                    throw new ArgumentNullException($"{nameof(bank.BankName)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.AccountHolderName))
                    throw new ArgumentNullException($"{nameof(bank.AccountHolderName)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.AccountNumber))
                    throw new ArgumentNullException($"{nameof(bank.AccountNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.IfscCode))
                    throw new ArgumentNullException($"{nameof(bank.IfscCode)} is mandatory");

                if (!Enum.IsDefined(typeof(AccountType), bank.AccountType))
                    throw new InvalidEnumArgumentException($"{nameof(bank.AccountType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IEmailAddress> emailAddresses)
        {
            if (emailAddresses == null || !emailAddresses.Any())
                throw new ArgumentNullException($"Applicant {nameof(emailAddresses)} is mandatory");

            foreach (var email in emailAddresses)
            {
                if (string.IsNullOrWhiteSpace(email.Email))
                    throw new ArgumentNullException($"{nameof(email.Email)} is mandatory");
                if (!Regex.IsMatch(email.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    throw new ArgumentException("Email Address is not valid");
                if (!Enum.IsDefined(typeof(EmailType), email.EmailType))
                    throw new InvalidEnumArgumentException($"{nameof(email.EmailType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IAddress> addresses)
        {
            if (addresses == null || !addresses.Any())
                throw new ArgumentNullException($"Applicant {nameof(addresses)} is mandatory");

            foreach (var address in addresses)
            {
                if (string.IsNullOrWhiteSpace(address.AddressLine1))
                    throw new ArgumentNullException($"Applicant {nameof(address.AddressLine1)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.City))
                    throw new ArgumentNullException($"Applicant {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.State))
                    throw new ArgumentNullException($"Applicant {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.ZipCode))
                    throw new ArgumentNullException($"Applicant {nameof(address.ZipCode)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.Country))
                    throw new ArgumentNullException($"Applicant {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined(typeof(AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException($"{nameof(address.AddressType)} is not valid");

                if (!Enum.IsDefined(typeof(LocationType), address.LocationType))
                    throw new InvalidEnumArgumentException($"{nameof(address.LocationType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IPhoneNumber> phoneNumbers)
        {
            if (phoneNumbers == null || !phoneNumbers.Any())
                throw new ArgumentNullException($"Applicant {nameof(phoneNumbers)} is mandatory");

            foreach (var phone in phoneNumbers)
            {
                string phoneNumberPattern = "^(0|[1-9][0-9]+)$";
                if (!Regex.IsMatch(phone.Phone, phoneNumberPattern))
                    throw new ArgumentException("Invalid Phone Number format");
                if (string.IsNullOrWhiteSpace(phone.Phone))
                    throw new ArgumentNullException($"Applicant {nameof(phone.Phone)} is mandatory");

                if (!Enum.IsDefined(typeof(PhoneType), phone.PhoneType))
                    throw new InvalidEnumArgumentException($"{nameof(phone.PhoneType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IOwner> owners)
        {
            if (owners == null || !owners.Any())
                throw new ArgumentNullException($"Applicant {nameof(owners)} is mandatory");

            foreach (var owner in owners)
            {
                if (string.IsNullOrWhiteSpace(owner.FirstName))
                    throw new ArgumentNullException($"{nameof(owner.FirstName)} is mandatory");
                if (string.IsNullOrWhiteSpace(owner.SSN))
                    throw new ArgumentNullException($"{nameof(owner.SSN)} is mandatory");
                if (string.IsNullOrWhiteSpace(owner.DOB))
                    throw new ArgumentNullException($"{nameof(owner.DOB)} is mandatory");

               /* if (string.IsNullOrWhiteSpace(owner.OwnershipPercentage))
                    throw new ArgumentNullException($"{nameof(owner.OwnershipPercentage)} is mandatory");*/

                // Code Commented for Update Owner consent related issue  as it's not required in case of FC
                /* if (string.IsNullOrWhiteSpace(owner.Designation))
                      throw new ArgumentNullException($"{nameof(owner.Designation)} is mandatory");
                    
                  if (!Enum.IsDefined(typeof(OwnershipType), owner.OwnershipType))
                      throw new InvalidEnumArgumentException($"{nameof(owner.OwnershipType)} is not valid");   */
            }
        }

        private string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }

        #endregion Private Methods - Validations
    }
}