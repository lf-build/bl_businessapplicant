﻿using System;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using LendFoundry.Business.Applicant.Api.Controllers;
using Moq;
using Xunit;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;

using LendFoundry.Business.Applicant.Fakes;

namespace LendFoundry.Business.Applicant.Api.Tests
{
    public class ApplicantControllerTest
    {
        #region Public Methods

        [Fact]
        public async void AddApplicantReturnApplicantIdOnValidApplicant()
        {
            var applicantService = GetApplicantService();
            var applicantRequest = GetApplicationRequestData();
            var response = (ObjectResult)await GetController(applicantService).Add(applicantRequest);
            Assert.IsType<HttpOkObjectResult>(response);
            var aplicantResponse = ((HttpOkObjectResult)response).Value as IApplicant;

            Assert.NotNull(aplicantResponse);
            Assert.NotNull(aplicantResponse.Id);
            Assert.NotEmpty(aplicantResponse.Id);
            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public async void AddApplicantReturnErrorIfUserAlreadyExists()
        {
            var applicantService = GetApplicantService();
            var applicantRequest = GetApplicationRequestData();
            applicantRequest.UserName = "TestUserExists";
            var response = (ObjectResult)await GetController(applicantService).Add(applicantRequest);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(409, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AssociateUser("Applicant1", "User2");
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void AssociateUserWithApplicatReturnBadRequestWhenUserAlreadyAssociated()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.UserId = "User1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser("Applicant1", "User1");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicat_ThrowsExceptionWithInvalidInputs()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.UserId = null;
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser(null, "User2");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicat_ThrowsExceptionWithInvalidUserInput()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.UserId = null;
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser("Application1", " ");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetBanksToApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.BankInformation[0].BankId = "b01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var banks = new List<BankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "343434343434",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetBanks("Applicant1", banks);
            var result = ((IActionResult)response) as ErrorResult;
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetBanksReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var banks = new List<BankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetBanks("Applicant1", banks);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetBanksReturnNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var banks = new List<BankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "11111111111",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetBanks("Applicant2", banks);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        //[Fact]
        //public async void SetBanksReturnNotFoundForBankIdNotExists()
        //{
        //    var applicantData = GetApplicationRequestData();
        //    applicantData.Id = "Applicant1";
        //    applicantData.BankInformation[0].BankId = "b02";
        //    var applicants = new List<IApplicant>();
        //    applicants.Add(applicantData);

        //    var banks = new List<BankInformation>
        //      {
        //          {
        //             new BankInformation()
        //                {
        //                    BankId = "b01",
        //                    BankName = "HDFC Bank",
        //                    AccountHolderName = "Sigma Info Solutions",
        //                    AccountNumber = "1111111111",
        //                    AccountType = AccountType.Savings,
        //                    IfscCode = "ifsccode111",
        //                    IsVerified = false
        //                }
        //            },
        //              new BankInformation()
        //                {
        //                    BankName = "Canara Bank",
        //                    AccountHolderName = "Sigma Info",
        //                    AccountNumber = "222222222222",
        //                    AccountType = AccountType.Savings,
        //                    IfscCode = "ifsccode111",
        //                    IsVerified = false
        //                }
        //            };

        //    var fakeApplicantRepository = GetApplicantRepository(applicants);
        //    var applicantService = GetApplicantService(fakeApplicantRepository);
        //     var response = await GetController(applicantService).SetBanks("Applicant1", banks);

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(404, result.StatusCode);
        //}

        [Fact]
        public async void SetAddressesToApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Addresses[0].AddressId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var addresses = new List<Address>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                    AddressId = "02",
                    AddressLine1 = "AddressUpdate1",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetAddresses("Applicant1", addresses);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetAddressesReturnBadRequestForInvlidData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Addresses[0].AddressId = "01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var addresses = new List<Address>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                    AddressId = "02",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetAddresses("Applicant1", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetAddressesNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Addresses[0].AddressId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var addresses = new List<Address>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "AddressUpdate1",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetAddresses("App123", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetAddressesNotFoundForAddressIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Addresses[0].AddressId = "01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var addresses = new List<Address>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "AddressUpdate1",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetAddresses("Applicant1", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesToApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.EmailAddresses[0].Id = "e01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var emailAddresses = new List<EmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetEmailAddresses("Applicant1", emailAddresses);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetEmailAddressesReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.EmailAddresses[0].Id = "e01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var emailAddresses = new List<EmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetEmailAddresses("Applicant1", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.EmailAddresses[0].Id = "e01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var emailAddresses = new List<EmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetEmailAddresses("Applicant111", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesNotFoundForIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.EmailAddresses[0].Id = "e01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var emailAddresses = new List<EmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e1101",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetEmailAddresses("Applicant1", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetOwnerToApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Owners[0].OwnerId = "o1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var Owners = new List<Owner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetOwners("Applicant1", Owners);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetOwnerReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Owners[0].OwnerId = "o1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var Owners = new List<Owner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetOwners("Applicant1", Owners);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetOwnerNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Owners[0].OwnerId = "o1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var Owners = new List<Owner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetOwners("Applicant111", Owners);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetOwnerNotFoundForOwnerIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.Owners[0].OwnerId = "o1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var Owners = new List<Owner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                    new Owner()
                    {
                        OwnerId ="o2",
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetOwners("Applicant1", Owners);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetPhoneNumbersToApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.PhoneNumbers[0].PhoneId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var phoneNumbers = new List<PhoneNumber>
                    {
                        new PhoneNumber
                        {
                            PhoneId = "02",
                            Phone = "978812121212",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        },
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetPhoneNumbers("Applicant1", phoneNumbers);
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetPhoneNumbersReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.PhoneNumbers[0].PhoneId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var phoneNumbers = new List<PhoneNumber>
                    {
                        new PhoneNumber
                        {
                            PhoneId = "02",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        },
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetPhoneNumbers("Applicant1", phoneNumbers);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetPhoneNumbersNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.PhoneNumbers[0].PhoneId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var phoneNumbers = new List<PhoneNumber>
                    {
                        new PhoneNumber
                        {
                            PhoneId = "02",
                            Phone = "978812121212",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        },
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        }
                };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).SetPhoneNumbers("Applicant123", phoneNumbers);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void GetApplicantSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).Get("Applicant1");
            Assert.IsType<HttpOkObjectResult>(response);
        }

        [Fact]
        public async void GetApplicantReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).Get("");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void GetApplicantNotFoundForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).Get("Applicant11");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void UpdateApplicatSuccess()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var updateApplicantRequest = new UpdateApplicantRequest()
            {
                LegalBusinessName = "TestUpdate",
                DBA = "Test",
                BusinessTaxID = "T",
                SICCode = "12345Update",
                DUNSNumber = "45345345Update",
                BusinessWebsite = "Testupdate",
                PrimaryFax = "UpdatedFax",
                BusinessType = BusinessType.LLC,

                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateApplicant("Applicant1", updateApplicantRequest);
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void UpdateApplicatReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var updateApplicantRequest = new UpdateApplicantRequest()
            {
                LegalBusinessName = "TestUpdate",
                DBA = "Test",
                BusinessTaxID = "T",
                SICCode = "12345Update",
                DUNSNumber = "45345345Update",
                BusinessWebsite = "Testupdate",
                PrimaryFax = "UpdatedFax",
                BusinessType = BusinessType.LLC,

                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateApplicant(null, updateApplicantRequest);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void DeleteReturnBadRequestForInvliadData()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).Delete("");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void DeleteReturnForApplicantIdNotExists()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).Delete("Applicant2");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        #endregion Public Methods

        #region Private Methods

        private ApplicantController GetController(IApplicantService applicantService)
        {
            return new ApplicantController(applicantService);
        }

        private static IApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static IApplicantService GetApplicantService(IApplicantRepository fakeApplicantRepository, IEnumerable<IApplicant> applicants = null)
        {
            return new ApplicantService(fakeApplicantRepository, Mock.Of<IEventHubClient>());
        }

        private static IApplicantRepository GetApplicantRepository(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantRepository(Mock.Of<ITenantTime>(), applicants);
        }

        private ApplicantRequest GetApplicationRequestData()
        {
            var applicant = new ApplicantRequest()
            {
                BusinessTaxID = "123456789",
                BusinessWebsite = "abc.com",
                LegalBusinessName = "XYZ Corporation",
                DBA = "Testdata",
                SICCode = "12345",
                DUNSNumber = "45345345",
                PrimaryFax = "123456",
                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                },

                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                     AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                }
                },

                BusinessStartDate = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                BusinessType = BusinessType.LLC,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                      Phone = "9898989898",
                      PhoneType = PhoneType.Mobile,
                      CountryCode = "91"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                    new BankInformation()
                    {
                        BankName = "HDFC",
                        AccountNumber= "10004567",
                        AccountHolderName="SigmaInfo",
                        IfscCode= "Ifsccode100",
                        AccountType=AccountType.Savings,
                    }
                },
                Owners = new List<IOwner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            }
            };
            return applicant;
        }

        #endregion Private Methods
    }
}