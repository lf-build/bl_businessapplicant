﻿using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;

using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using System.ComponentModel;
using LendFoundry.Business.Applicant;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Identity;
using LendFoundry.Business.Applicant.Fakes;

namespace LendFoundry.Business.Applicant.Tests
{
    public class ApplicantServiceTests
    {
        #region Public Methods

        [Fact]
        public async void ApplicantService_Add()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var applicant = GetApplicationRequestData();
            var identityService = new Mock<LendFoundry.Security.Identity.Client.IIdentityService>();
            IUser user = new User()
            {
                Email = applicant.PrimaryEmail.Email,
                Name = applicant.Owners[0].FirstName + " " + applicant.Owners[0].LastName,
                Username = applicant.UserName,
                Password = applicant.Password,
                Roles = new string[] { "Applicant" }
            };
            identityService.Setup(c => c.CreateUser(It.IsAny<User>())).ReturnsAsync(new UserInfo() { Id = "123456" });

            var service = GetApplicantService(fakeApplicantRepository, identityService);

            var applicantData = await service.Add(applicant);
            Assert.Equal("123456", applicantData.UserId);
        }

        [Fact]
        public async void ApplicantService_Add_ThrowExceptionWhenRequestIsNull()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
        }

        [Fact]
        public async void ApplicantService_Add_ThowException()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);
            var applicant = new ApplicantRequest();

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.UserName = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.Password = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.UserId = "123";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.LegalBusinessName = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.DBA = "test";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
            applicant.BusinessType = BusinessType.LLC;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.SICCode = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.BusinessWebsite = "test.com";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryFax = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryPhone = new PhoneNumber();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryPhone.Phone = "123456";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
            applicant.PrimaryPhone.PhoneType = PhoneType.Mobile;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryEmail = new EmailAddress();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryEmail.Email = "abc@gmail.com";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
            applicant.PrimaryEmail.EmailType = EmailType.Personal;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress = new Address();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.AddressLine1 = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.City = "Ahmedabad";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.State = "Gujarat";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.ZipCode = "380002";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.Country = "india";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.AddressType = AddressType.Business;
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
            applicant.PrimaryAddress.LocationType = LocationType.Ahmedabad;
            //  await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenAddressIsNull()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);
            var applicant = GetApplicationRequestData();
            applicant.Addresses = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses = new List<IAddress>();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses.Add(new Address() { AddressLine1 = "Test" });
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses[0].City = "Ahmedabad";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses[0].State = "Gujrat";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses[0].ZipCode = "380006";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Addresses[0].Country = "india";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));

            applicant.Addresses[0].AddressType = AddressType.Business;
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));

            // applicant.Addresses[0].LocationType = LocationType.Ahmedabad;
            // await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenPhoneNumberIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = GetApplicationRequestData();
            applicant.PhoneNumbers = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.PhoneNumbers = new List<IPhoneNumber>();
            applicant.PhoneNumbers.Add(new PhoneNumber() { });
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.PhoneNumbers[0].Phone = "98989898";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));

            //   applicant.PhoneNumbers[0].PhoneType = PhoneType.Mobile;
            //   await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenEmailIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = GetApplicationRequestData();
            applicant.EmailAddresses = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.EmailAddresses = new List<IEmailAddress>();
            applicant.EmailAddresses.Add(new EmailAddress());
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.EmailAddresses[0].Email = "abc@gmail.com";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenOwnerIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = GetApplicationRequestData();
            applicant.Owners = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Owners = new List<IOwner>();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Owners.Add(new Owner() { FirstName = "Test" });
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.Owners[0].SSN = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.Owners[0].DOB = "2016-09-02T15:24:28.2480918+05:30";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.Owners[0].Ownership = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.Owners[0].Designation = "CEO";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenBankInfoIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = GetApplicationRequestData();
            applicant.BankInformation = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.BankInformation = new List<IBankInformation>();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.BankInformation.Add(new BankInformation() { BankName = "Test" });
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.BankInformation[0].AccountHolderName = "Test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));

            applicant.BankInformation[0].AccountNumber = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
            applicant.BankInformation[0].IfscCode = "123456789";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThrowExceptionWhenUserIsNull()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var identityService = new Mock<LendFoundry.Security.Identity.Client.IIdentityService>();
            identityService.Setup(c => c.CreateUser(It.IsAny<User>())).ReturnsAsync(new UserInfo());
            var applicant = GetApplicationRequestData();
            var service = GetApplicantService(fakeApplicantRepository, identityService);

            await Assert.ThrowsAsync<UnableToCreateUserException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_AssociateUser()
        {
            var applicantData = GetApplicationRequestData();
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = new Mock<IApplicantRepository>();
            var service = GetApplicantService(fakeApplicantRepository.Object);

            await service.AssociateUser("Applicant1", "User1");
            fakeApplicantRepository.Verify(x => x.AssociateUser(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void ApplicantService_AssociateUser_ThrowsExceptionWhenUserIsAlreadyAssociated()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.UserId = "user1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<UserAlreadyAssociatedException>(async () => await service.AssociateUser("applicant1", "User1"));
        }

        [Fact]
        public async void ApplicantService_AssociateUser_ThrowsExceptionWithInvalidAruguments()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "Applicant1";
            applicantData.UserId = "User4";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("Applicant1", ""));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("", "User1"));
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.AssociateUser("Applicant5", "User1"));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser(null, "User1"));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("Applicant1", null));
        }

        [Fact]
        public async void ApplicantService_SetPhoneNumbers()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.PhoneNumbers[0].PhoneId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var phoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        Phone = "978821232123",
                        PhoneType = PhoneType.Mobile,
                        CountryCode = "01"
                    } };

            await service.SetPhoneNumbers("applicant1", phoneNumbers);
            Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().PhoneNumbers.Count);
            Assert.Equal(PhoneType.Residence, fakeApplicantRepository.Applicants.ToList().First().PhoneNumbers.Where(p => p.PhoneId == "02").ToList().FirstOrDefault().PhoneType);
        }

        [Fact]
        public async void ApplicantService_SetPhoneNumbers_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var phoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneType = PhoneType.Mobile,
                        CountryCode = "01"
                    } };

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetPhoneNumbers(null, phoneNumbers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetPhoneNumbers("Applicant1", null));
            phoneNumbers[1].Phone = "98989898";
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.SetPhoneNumbers("Applicant1", phoneNumbers));
        }

        [Fact]
        public async void ApplicantService_SetAddresses()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.Addresses[0].AddressId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "AddressUpdate1",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };

            await service.SetAddresses("applicant1", addresses);
            Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().Addresses.Count);
            Assert.Equal(AddressType.Business, fakeApplicantRepository.Applicants.ToList().First().Addresses.Where(p => p.AddressId == "02").ToList().FirstOrDefault().AddressType);
        }

        [Fact]
        public async void ApplicantService_SetAddresses_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                      LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                    AddressId = "02",
                    AddressLine1 = "AddressLine1",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                      LocationType = LocationType.Ahmedabad
                } };

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetAddresses(null, addresses));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetAddresses("Applicant1", null));
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.SetAddresses("Applicant1", addresses));
        }

        [Fact]
        public async void ApplicantService_SetEmailAddresses()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.EmailAddresses[0].Id = "e01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var emailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            await service.SetEmailAddresses("applicant1", emailAddresses);
            Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().EmailAddresses.Count);
            Assert.Equal("testUpdate@gmail.com", fakeApplicantRepository.Applicants.ToList().First().EmailAddresses.Where(p => p.Id == "e01").ToList().FirstOrDefault().Email);
        }

        [Fact]
        public async void ApplicantService_SetEmailAddresses_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var emailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetEmailAddresses(null, emailAddresses));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetEmailAddresses("Applicant1", null));
        }

        [Fact]
        public async void ApplicantService_SetOwners()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.Owners[0].OwnerId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var employment = new List<IOwner>
                {
                   new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "40%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
                };
            await service.SetOwner("applicant1", employment);
            Assert.Equal(3, fakeApplicantRepository.Applicants.ToList().First().Owners.Count);
            Assert.Equal("Owner1", fakeApplicantRepository.Applicants.ToList().First().Owners.Where(e => e.OwnerId == "02").First().FirstName);
        }

        [Fact]
        public async void ApplicantService_SetOwners_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.Owners[0].OwnerId = "02";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var employment = new List<IOwner>
                {
                   new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "40%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
                };
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOwner(null, employment));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOwner("Applicant1", null));
        }

        [Fact]
        public async void ApplicantService_SetBanks()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            applicantData.BankInformation[0].BankId = "b01";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var banks = new List<IBankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };
            await service.SetBanks("applicant1", banks);
            Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().BankInformation.Count);
            Assert.Equal("Sigma Info Solutions", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().AccountHolderName);
            Assert.Equal("1212121212121212", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().AccountNumber);
            Assert.Equal("HDFC Bank", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().BankName);
        }

        [Fact]
        public async void ApplicantService_SetBanks_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var banks = new List<IBankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetBanks(null, banks));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetBanks("Applicant1", null));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetBanks("Applicant1", banks));
        }

        [Fact]
        public async void ApplicantService_UpdateApplicant()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var updateApplicantRequest = new UpdateApplicantRequest()
            {
                LegalBusinessName = "TestUpdate",
                DBA = "Test",
                BusinessTaxID = "T",
                SICCode = "12345Update",
                DUNSNumber = "45345345Update",
                BusinessWebsite = "Testupdate",
                PrimaryFax = "UpdatedFax",
                BusinessType = BusinessType.LLC,

                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                }
            };

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await service.UpdateApplicant("applicant1", updateApplicantRequest);
            Assert.Equal("12345Update", fakeApplicantRepository.Applicants.ToList().First().SICCode);
            Assert.Equal("45345345Update", fakeApplicantRepository.Applicants.ToList().First().DUNSNumber);
            Assert.Equal(BusinessType.LLC, fakeApplicantRepository.Applicants.ToList().First().BusinessType);
        }

        [Fact]
        public async void ApplicantService_UpdateApplicant_ThrowsWhenInputsAreInvalid()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);
            var applicant = new UpdateApplicantRequest();

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.LegalBusinessName = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.DBA = "test";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.BusinessType = BusinessType.LLC;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.SICCode = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.BusinessWebsite = "test.com";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryFax = "123456";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryPhone = new PhoneNumber();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryPhone.Phone = "123456";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryPhone.PhoneType = PhoneType.Mobile;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryEmail = new EmailAddress();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryEmail.Email = "abc@gmail.com";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryEmail.EmailType = EmailType.Personal;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress = new Address();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.AddressLine1 = "test";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.City = "Ahmedabad";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.State = "Gujarat";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.ZipCode = "380002";
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.Country = "india";
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("application1", applicant));
            applicant.PrimaryAddress.AddressType = AddressType.Business;
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("application1", applicant));
        }

        [Fact]
        public async void ApplicantService_GetApplicantById()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var Result = await service.Get("applicant1");
            Assert.Equal("applicant1", Result.Id);
            Assert.Equal("Testdata", Result.DBA);
        }

        [Fact]
        public async void ApplicantService_GetApplicantById_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.Get(null));
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.Get("applicant2"));
        }

        [Fact]
        public async void ApplicantService_Delete()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();
            applicants.Add(applicantData);

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await service.Delete("applicant1");
            Assert.Equal(0, fakeApplicantRepository.Applicants.ToList().Count);
        }

        [Fact]
        public async void ApplicantService_Delete_ThrowsWhenInputsAreInvalid()
        {
            var applicantData = GetApplicationRequestData();
            applicantData.Id = "applicant1";
            var applicants = new List<IApplicant>();

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.Delete(null));
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.Delete("applicant2"));
        }

        #endregion Public Methods

        #region Private Methods

        private static FakeApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static FakeApplicantRepository GetFakeApplicantRepository(List<IApplicant> applicants = null)
        {
            return new FakeApplicantRepository(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static IApplicantService GetApplicantService(IApplicantRepository fakeApplicantRepository)
        {
            return new ApplicantService(fakeApplicantRepository, Mock.Of<IEventHubClient>());
        }

        private static IApplicantService GetApplicantService(IApplicantRepository fakeApplicantRepository, Mock<LendFoundry.Security.Identity.Client.IIdentityService> IdentityService)
        {
            return new ApplicantService(fakeApplicantRepository, Mock.Of<IEventHubClient>());
        }

        private ApplicantRequest GetApplicationRequestData()
        {
            var applicant = new ApplicantRequest()
            {
                BusinessTaxID = "123456789",
                BusinessWebsite = "abc.com",
                LegalBusinessName = "XYZ Corporation",
                DBA = "Testdata",
                SICCode = "12345",
                DUNSNumber = "45345345",
                PrimaryFax = "123456",
                SocialLinks = new SocialLinksRequest()
                {
                    FacebookAddress = "test@gmail.com",
                    GoogleAddress = "test",
                    YelpAddress = "test"
                },

                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                },

                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                     AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                }
                },

                BusinessStartDate = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                BusinessType = BusinessType.LLC,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                      Phone = "9898989898",
                      PhoneType = PhoneType.Mobile,
                      CountryCode = "91"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                    new BankInformation()
                    {
                        BankName = "HDFC",
                        AccountNumber= "10004567",
                        AccountHolderName="SigmaInfo",
                        IfscCode= "Ifsccode100",
                        AccountType=AccountType.Savings,
                    }
                },

                Owners = new List<IOwner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            }
            };
            return applicant;
        }

        #endregion Private Methods
    }
}