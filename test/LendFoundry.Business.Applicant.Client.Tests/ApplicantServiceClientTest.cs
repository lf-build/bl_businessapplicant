﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using Xunit;
using Moq;

using LendFoundry.Foundation.Services;

namespace LendFoundry.Business.Applicant.Client.Tests
{
    public class ApplicantServiceClientTests
    {
        #region Constructors

        public ApplicantServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicantServiceClient = new ApplicantService(MockServiceClient.Object);
        }

        #endregion Constructors

        #region Public Methods

        [Fact]
        public async void Client_Add_Applicant()
        {
            var applicantRequest = GetApplicationRequestData();
            MockServiceClient.Setup(s => s.ExecuteAsync<Applicant>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .ReturnsAsync(new Applicant());

            var result = await ApplicantServiceClient.Add(applicantRequest);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_AssociateUser()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.AssociateUser("ApplicantId", "UserId");
            Assert.Equal("{applicantId}/associate/user/{userId}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_Get()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<Applicant>(It.IsAny<IRestRequest>()))
              .Callback<IRestRequest>(r => Request = r)
              .ReturnsAsync(new Applicant());

            await ApplicantServiceClient.Get("ApplicantId");
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public async void Client_SetAddresses()
        {
            var addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "AddressUpdate1",
                    AddressLine2 = "Address2",
                    City = "Surat",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Business,
                     LocationType = LocationType.Ahmedabad
                }
                };
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.SetAddresses("ApplicantId", addresses);
            Assert.Equal("{applicantId}/address", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_SetPhoneNumbers()
        {
            var phoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                            PhoneId = "02",
                            Phone = "978812121212",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        },
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        }
                };

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.SetPhoneNumbers("ApplicantId", phoneNumbers);
            Assert.Equal("{applicantId}/phonenumbers", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_SetEmailAddresses()
        {
            var emailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    },
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                    },
                };

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.SetEmailAddresses("ApplicantId", emailAddresses);
            Assert.Equal("{applicantId}/emailaddress", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_SetOwner()
        {
            var Owners = new List<IOwner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner1@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                },
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner2@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner2",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            };

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.SetOwner("ApplicantId", Owners);
            Assert.Equal("{applicantId}/owners", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_SetBanks()
        {
            var banks = new List<IBankInformation>
              {
                  {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "11111111111",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    },
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                            IsVerified = false
                        }
                    };
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.SetBanks("ApplicantId", banks);
            Assert.Equal("{applicantId}/banks", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_UpdateApplicant()
        {
            var updateApplicantRequest = new UpdateApplicantRequest()
            {
                LegalBusinessName = "TestUpdate",
                DBA = "Test",
                BusinessTaxID = "T",
                SICCode = "12345Update",
                DUNSNumber = "45345345Update",
                BusinessWebsite = "Testupdate",
                PrimaryFax = "UpdatedFax",
                BusinessType = BusinessType.LLC,

                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                }
            };
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.UpdateApplicant("ApplicantId", updateApplicantRequest);
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_Delete()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.Delete("ApplicantId");
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }

        #endregion Public Methods

        #region Private Methods

        private ApplicantService ApplicantServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }

        #endregion Private Methods

        private ApplicantRequest GetApplicationRequestData()
        {
            var applicant = new ApplicantRequest()
            {
                BusinessTaxID = "123456789",
                BusinessWebsite = "abc.com",
                LegalBusinessName = "XYZ Corporation",
                DBA = "Testdata",
                SICCode = "12345",
                DUNSNumber = "45345345",
                PrimaryFax = "123456",
                PrimaryPhone = new PhoneNumber()
                {
                    Phone = "9596889475",
                    PhoneType = PhoneType.Mobile
                },
                PrimaryAddress = new Address()
                {
                    AddressLine1 = "Employeertest1",
                    AddressLine2 = "Employeertest2",
                    AddressLine3 = null,
                    AddressLine4 = null,
                    LandMark = null,
                    City = "Ahmedabad",
                    State = "Gujarat",
                    ZipCode = "380034",
                    Country = "India",
                    AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                },
                PrimaryEmail = new EmailAddress()
                {
                    Email = "test@gmail.com",
                    EmailType = EmailType.Work
                },

                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    ZipCode = "380034",
                    State = "Gujarat",
                     AddressType = AddressType.Business,
                    LocationType = LocationType.Ahmedabad
                }
                },

                BusinessStartDate = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                BusinessType = BusinessType.LLC,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                      Phone = "9898989898",
                      PhoneType = PhoneType.Mobile,
                      CountryCode = "91"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                    new BankInformation()
                    {
                        BankName = "HDFC",
                        AccountNumber= "10004567",
                        AccountHolderName="SigmaInfo",
                        IfscCode= "Ifsccode100",
                        AccountType=AccountType.Savings,
                    }
                },
                Owners = new List<IOwner>
                {
                    new Owner()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {
                                 AddressLine1 = "Employeertest1",
                                 AddressLine2 = "Employeertest2",
                                 City = "Ahmedabad",
                                 Country = "India",
                                 ZipCode = "380034",
                                  AddressType = AddressType.Business,
                               LocationType = LocationType.Ahmedabad
                            }
                        },
                    EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "owner@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                    }
                },
                    Designation = "Manager",
                    FirstName= "Owner1",
                    OwnershipType = OwnershipType.OwnershipType1,
                    SSN = "123456",
                    DOB = "2016-09-02T15:24:28.2480918+05:30",
                    Ownership = "60%",
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber
                        {
                          Phone = "9898989898",
                          PhoneType = PhoneType.Work,
                          CountryCode = "91"
                        }
                    }
                }
            }
            };
            return applicant;
        }
    }
}